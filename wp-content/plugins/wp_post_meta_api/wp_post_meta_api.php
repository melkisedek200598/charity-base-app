<?php
	/**
	 * @package WordPress Post Meta API Restful
	 * @version 1.0
	**/
	 
	/*
	Plugin Name: WordPress Post Meta API Restful
	Plugin URI: adinandra.dharmasurya.net
	Description: Add RESTful API to manage meta data(s)
	Author: @nandcep
	Version: 1.0
	Author URI: adinandra.dharmasurya.net
	*/
	add_action(  'rest_api_init', 'create_api_posts_meta_field');

	function create_api_posts_meta_field() {


 		register_rest_field( 'post', 'post-meta-fields', array(
				'get_callback' => 'get_post_meta_for_api',
				'schema' => null,
			)
		);

		register_rest_route( 'custom/v1', '/meta', array(
				'methods' => 'POST',
				'callback' => 'merge_post_meta_for_api',
			) 
		);
	}
 
	function get_post_meta_for_api( $object ) {
		$post_id = $object['id'];
		return get_post_meta( $post_id );
	}

	function merge_post_meta_for_api($request){
		$params = $request->get_body_params();
		$post_id = $params['post_id'];
		$key = $params['key'];
		$value = $params['value'];
		$is_postmeta_exists = get_post_meta($post_id, $key);
		$response = '';
		if($is_postmeta_exists != NULL){
			update_post_meta( $post_id, $key, $value );
			$response = array("update" => $post_id);
		}
		else{
			add_post_meta( $post_id, $key, $value );
			$response = array("create" => $post_id);
		}
		return new WP_REST_Response( $response, 200 );
	}

?>